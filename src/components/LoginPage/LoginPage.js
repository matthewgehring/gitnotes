import React from "react";
import styled from "styled-components";
import scribble_big from "../../images/scribble_big.png";
import scribble_small from "../../images/scribble_small.png";
import gitlab from "../../images/gitlab.svg";
import github from "../../images/github.svg";
import bitbucket from "../../images/bitbucket.svg";
import { colors } from "../../styles/styles";

const Wrapper = styled.div`
  min-height: calc(100vh - 50px);
  width: 100%;
  min-width: 640px;
  background-color: #f8f8f8;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  padding: 0 50px 50px 50px;
  margin: 0 auto;

  h2 {
    color: white;
    margin-top: 0;
  }

  @media only screen and (max-width: 960px) {
    flex-direction: column;
    padding: 0 10px;
  }
`;

const TopContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin: 50px 0;
  max-width: 1300px;

  @media only screen and (max-width: 960px) {
    flex-direction: column;
  }
`;

const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  margin: 0 auto;
  height: 300px;

  h3 {
    margin-bottom: 0;
  }

  @media only screen and (max-width: 960px) {
    margin: 50px 0;
  }
`;

const Button = styled.button`
  width: 250px;
  height: 60px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 15px;
  background-color: ${props => props.bg};
  color: white;
  border: none;
  border-radius: 10px;
  font-size: 18px;
  box-shadow: 5px 10px 10px rgba(0, 0, 0, 0.3);

  p {
    margin: 0;
  }

  img {
    height: 45px;
    width: 45px;
  }

  &:hover {
    filter: brightness(1.3);
    cursor: pointer;
  }
`;

const Card = styled.div`
  background-color: ${colors.theme1};
  height: 300px;
  width: 500px;
  color: white;
  padding: 20px;
  border-radius: 10px;
  box-shadow: 5px 10px 10px rgba(0, 0, 0, 0.3);
  display: flex;
  flex-direction: column;
  margin: 0 auto;

  h1 {
    margin: 0;
  }

  div {
    align-self: flex-end;
    width: 85%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    align-items: flex-start;

    img {
      margin: 0;
    }
  }
`;

const GraphicContainer = styled.div`
  width: 98%;
  max-width: 1300px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;

  p {
    width: 180px;
    font-size: 16px;
    text-align: center;
  }
`;
const TextContainer = styled.div`
  width: 100%;
  max-width: 1300px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;

  p {
    width: 180px;
    font-size: 16px;
    text-align: center;
  }
`;

const Circle = styled.div`
  height: 130px;
  width: 130px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 10;
  background-color: ${colors.theme1};
  border-radius: 50%;
  box-shadow: 5px 10px 10px rgba(0, 0, 0, 0.3);

  span {
    height: 80px;
    width: 80px;
    font-size: 60px;
    padding: 0;
    text-align: center;
    margin-top: 10px;
  }
`;

const Line = styled.span`
  position: absolute;
  margin: auto;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  height: 8px;
  width: 95%;
  background-color: ${colors.theme1};
  z-index: 5;
  box-shadow: 5px 10px 10px rgba(0, 0, 0, 0.3);
`;

const LoginPage = ({ _authenticate }) => (
  <Wrapper>
    <TopContainer>
      <Card>
        <h1>GitNotes</h1>
        <div>
          <img alt="gitnotes_scribble" src={scribble_small} />
          <img alt="gitnotes_scribble" src={scribble_big} />
          <img alt="gitnotes_scribble" src={scribble_small} />
        </div>
      </Card>
      <ButtonsContainer>
        <h3>Choose your host..</h3>
        <Button bg="#2C295F" onClick={_authenticate}>
          <p>Gitlab</p>
          <img alt="gitlab" src={gitlab} />
        </Button>
        <Button bg="#3496F0">
          <p>Coming Soon</p>
          <img alt="bitbucket" src={bitbucket} />
        </Button>
        <Button bg="#24292E">
          <p>Coming Soon</p>
          <img alt="github" src={github} />
        </Button>
      </ButtonsContainer>
    </TopContainer>
    <GraphicContainer>
      <Circle>
        <span role="img" aria-label="Memo">
          &#x1F4DD;
        </span>
      </Circle>
      <Circle>
        <span role="img" aria-label="Rocket">
          &#x1F680;
        </span>
      </Circle>
      <Circle>
        <span role="img" aria-label="Party Popper">
          &#x1F389;
        </span>
      </Circle>
      <Line />
    </GraphicContainer>
    <TextContainer>
      <p>Create, edit and browse all of the notes in your git repository.</p>
      <p>Gain complete control over your notes and collaborate with other.</p>
      <p>The best part of all.. It's Free!</p>
    </TextContainer>
  </Wrapper>
);

export default LoginPage;
